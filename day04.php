<!DOCTYPE html>
<html lang="vn">
<head>
    <meta charset="UTF-8">
</head>
    <title>Sign Up</title>
<body>
    
    <fieldset style='width: 500px; height: 450px; margin: auto; margin-top: 20px; boder:#1E90FF solid'>
    <?php 
        $hoten = $gioitinh = $dob = $diachi="";
        $phankhoa = array("Toán cơ tin học" => "T", "Sinh học" => "S", "Vật lý" => "V");
         if($_SERVER["REQUEST_METHOD"]== "POST") {
            if(empty(inputHandling($_POST["hoten"]))) {
                echo "<div style='color: red;'>Hãy nhập họ tên</div>";
            }
            if (empty($_POST["gioitinh"])) {
                echo "<div style='color: red;'>Hãy chọn giới tính</div>";
            }
            if(empty(inputHandling($_POST["phankhoa"]))){
                echo "<div style='color: red;'>Hãy chọn khoa</div>";
            }    
            if(empty(inputHandling($_POST["dob"]))){
                echo "<div style='color: red;'>Hãy nhập ngày sinh</div>";
            }
            elseif (!validateDate($_POST["dob"])) {
                echo "<div style='color: red;'>Hãy nhập ngày sinh đúng định dạng</div>";
            }
        }
        function inputHandling($data) {
            $data = trim($data);
            $data = stripslashes($data);
            return $data;
        }
        function validateDate($date){
            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
                return true;
            } else {
                return false;
            }
        }
        
    ?>
    <form style='margin: 20px 50px 0 30px' method="post">
        <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
                <tr height = '40px'>
                    <td width = 30% style = 'background-color: #32CD32; 
                    vertical-align: top; text-align: center; padding: 5px 5px'>
                        <label style='color: white;'>Họ và tên<span style='color:red;'>*</span></label>
                    </td>
                    <td width = 30% >
                        <input type='text' hoten = "hoten" style = 'line-height: 34px; border-color:#32CD32'>
                    </td>
                </tr>
                <tr height = '40px'>
                    <td width = 30% style = 'background-color: #32CD32; vertical-align: top; text-align: center; padding: 5px 5px'>
                        <label style='color: white;'>Giới tính<span style='color:red;'>*</span></label>
                    </td>
                    <td width = 30% >
                    <?php
                        $gioitinh=array("Nam","Nữ");
                        for($i = 0; $i < count($gioitinh); $i++){
                            echo"
                                <label class='container'>
                                    <input type='radio' value=".$gioitinh[$i]." hoten='gioitinh'>"
                                    .$gioitinh[$i]. 
                                "</label>";
                        }
                    ?>  

                    </td>
                </tr>


                <tr height = '40px'>
                    <td style = 'background-color: #32CD32; vertical-align: top; text-align: center; padding: 5px 5px'>
                        <label style='color: white;'>Phân Khoa<span style='color:red;'>*</span></label>
                    </td>
                    <td height = '40px'>
                        <select hoten='phankhoa' style = 'border-color:#82cd79;height: 100%;width: 80%;'>
                            <?php
                                foreach($phankhoa as $item => $value){
                                echo "<option>".$item."</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>

                <tr height = '40px'>
                    <td style = 'background-color: #32CD32; vertical-align: top; text-align: center; padding: 5px 5px'>
                        <label style='color: white;'>Ngày sinh<span style='color:red;'>*</span></label>
                    </td>
                    <td height = '40px'>
                        <input type='date' hoten="dob" data-date="" data-date-format="DD MMMM YYYY" style = 'line-height: 32px; border-color:#82cd79'>
                    </td>
                </tr>
                <tr height = '40px'>
                    <td style = 'background-color: #32CD32; vertical-align: top; text-align: center; padding: 5px 5px'>
                        <label style='color: white;'>Địa chỉ</label>
                    </td>
                    <td height = '40px'>
                        <input type='text' hoten="diachi" style = 'line-height: 32px; border-color:#32CD32'> 
                    </td>
                </tr>
            </table>
            <button style='background-color: #32CD32; border-radius: 5px; 
            width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white;'>Đăng Kí</button>
        </form>
    </fieldset>
</body>
</html>